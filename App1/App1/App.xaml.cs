﻿using System;
using Xamarin.Forms;
using System.IO;
using Xamarin.Forms.Xaml;
using Model;
using SQLite;

namespace App1
{
    public partial class App : Application 
    {

        private static Database database;
        public static Database Database { get {
                if (database == null)

                { database = new Database(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Formules.db3")); }
                return database;
                
            } }

       

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
