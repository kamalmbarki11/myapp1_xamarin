﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using Model;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public ObservableCollection<MyIytems> Formule_admin { get; set; } = new ObservableCollection<MyIytems>();
        public Page2()
        {
               Formule_admin.Add(new MyIytems("formule 1 ", "E=m*c^2", "i3.ipg"));
            Formule_admin.Add(new MyIytems("formule 2", "p=m*g", "i3.ipg"));
            Formule_admin.Add(new MyIytems("formule 3", "v=d/t", "i3.ipg"));
            Formule_admin.Add(new MyIytems("formule 4", "v=s*h", "i3.ipg"));
            Formule_admin.Add(new MyIytems("formule 5", "six", "i3.ipg"));
            Formule_admin.Add(new MyIytems("formule 6", "dix", "i3.ipg"));
            Formule_admin.Add(new MyIytems("formule 7", "aix", "i3.ipg"));
            //ListFormule.ItemsSource =Formule_admin;
             // BindingContext = this;
            InitializeComponent();

        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            ListFormule.ItemsSource =await App.Database.Getformules();
        }

        private async void add_formule(object sender, EventArgs e) {
            string img = (picker_id.SelectedItem) as string+ ".PNG";
            if (picker_id.SelectedItem.ToString().StartsWith("L"))
            {   foreach(var f in (ListFormule.ItemsSource as List<MyIytems>)) {
                    if (f.Titre!= name_formule.Text) {
                        await App.Database.SaveFormule(new MyIytems {  Titre = name_formule.Text, Comment = formule.Text, Myimg = img });
                        break;         }
                    else { await DisplayAlert("Erreur ", "Cette formule existe deja", "cancel", "OK");
                        break;
                    }
                }


                //   await App.Database.DleteFormule(new MyIytems { ID=1, Titre = name_formule.Text, Comment = formule.Text, Myimg = img });
                ListFormule.ItemsSource = await App.Database.Getformules();


            }
            //Formule_admin.Add(new MyIytems(1,name_formule.Text, formule.Text, img)); }
            else
               await DisplayAlert("Erreur ", " selectionner un Niveau de formule ", "cancel", "OK");
        }
        private async void delete_formule(object sender, EventArgs e)
        {
            string img = (picker_id.SelectedItem) as string + ".PNG";
            if (string.IsNullOrEmpty(name_formule.Text) || string.IsNullOrEmpty(formule.Text)) { await DisplayAlert("Erreur ", " selectionner un bien le nom de formule ", "cancel", "OK"); }
            else {
                foreach (var f in ListFormule.ItemsSource) {
                    if ((f as MyIytems).Titre == name_formule.Text)
                    {
                        await App.Database.DleteFormule(new MyIytems { ID= (f as MyIytems).ID,
                            Titre = (f as MyIytems).Titre,
                            Comment = (f as MyIytems).Comment, Myimg = (f as MyIytems).Myimg + ".PNG"
                        });
                        

                    }

                }
                ListFormule.ItemsSource = await App.Database.Getformules();
            }
              
            //Formule_admin.Add(new MyIytems(1,name_formule.Text, formule.Text, img)); }
        }



    }
}