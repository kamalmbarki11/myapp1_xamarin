﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Details : ContentPage
    {
        public Details(string titre,string com,string img)
        {
            InitializeComponent();
            id_titre.Text= titre;
            id_expression.Text = com;
            ID_img.Source= img;
        }
    }
}