﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using Model;

namespace App1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public List<MyIytems> Myitems { get; set; } = new List<MyIytems>();
        public List<MyIytems> Myitems_Filtre { get; set; } = new List<MyIytems>();
        public Page1()
        {
            InitializeComponent();
            //Myitems.Add(new MyIytems("formule 1 ", "E=m*c^2","i3.ipg"));
           // Myitems.Add(new MyIytems("formule 2", "p=m*g", "i3.ipg"));
            //Myitems.Add(new MyIytems("formule 3", "v=d/t", "i3.ipg"));
           // Myitems.Add(new MyIytems("formule 4", "v=s*h", "i3.ipg"));
           // Myitems.Add(new MyIytems("formule 5", "six", "i3.ipg"));
           // Myitems.Add(new MyIytems("formule 6", "dix", "i3.ipg"));
          //  Myitems.Add(new MyIytems("formule 7", "aix", "i3.ipg"));
          //  Myitems_Filtre = Myitems;
            BindingContext = this;

        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            ListView1.ItemsSource = await App.Database.Getformules() ;
            Myitems= await App.Database.Getformules();
        }
      // juste pour tester au debut  private void Add_element(object sender, EventArgs e) { Myitems.Add(new MyIytems("formule n", "n=m/M","i3.jpg")); }
        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            //thats all I need to make a searchbox 

          
            ListView1.ItemsSource=Myitems.Where(x=>x.Titre.ToLower().StartsWith(e.NewTextValue.ToLower()));
         
        }

        async private void onItemClicked(object sender,ItemTappedEventArgs e) {

            var itemclicked =ListView1.SelectedItem;
            var details =e.Item as MyIytems;
            //var index = ListView1.ItemsSource.;// Myitems.IndexOf((MyIytems)itemclicked);
           // var titreclicked =Myitems[index].Titre;
            //var commentclicked = Myitems[index].Comment;
            //var imgsource = Myitems[index].Myimg;
            await Navigation.PushAsync(new Details(details.Titre,details.Comment,details.Myimg), true);
            

        }
        



    }
}