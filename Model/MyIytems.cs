﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using SQLite;
namespace App1
{
   public  class MyIytems : INotifyPropertyChanged
    {
        private string title="fjn";
        private string comment="cefidsn";
        private string myimg = "13.jpg";
      //  private int id = 0;

        [PrimaryKey,AutoIncrement]
        public int ID { get; set;  }
       public string Titre {
            get =>title;
            set {
                if (string.IsNullOrWhiteSpace(value)) return;
                title = value;
                PropertyChanged?.Invoke(this,new PropertyChangedEventArgs("Titre"));
            }
        }
        public string Comment
        {
            get => comment;
            set
            {
                if (string.IsNullOrWhiteSpace(value)) return;
                comment = value;
            }
        }

        public string Myimg {
            get => myimg;
            set
            {
                if (string.IsNullOrWhiteSpace(value)) return;
                myimg = value;
            }

        }

        public MyIytems(string title,string comment) {
           Titre=title;
           Comment=comment;
        }
        public MyIytems(string title, string comment,string img)
        {
            Titre = title;
            Comment = comment;
            Myimg = img;
        }
        public MyIytems()
        {
            _=ID;
            Titre = title;
            Comment = comment;
            Myimg = myimg;
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString() {   return $" {Titre} {Comment}"; }
    }
}
