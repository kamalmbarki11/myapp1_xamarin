﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using App1;
using SQLite;



namespace Model
{
    public  class Database
    {
        private readonly SQLiteAsyncConnection _db;

       public Database(string path)
        {    
            _db = new SQLiteAsyncConnection(path);
            _db.CreateTableAsync<MyIytems>();
        }

      
       public  Task<List<MyIytems>> Getformules() { return _db.Table<MyIytems>().ToListAsync();}
        public   Task<int> SaveFormule(MyIytems myformules) {
         
           return _db.InsertAsync(myformules);


             }
       
        public  Task<int> DleteFormule(MyIytems myformules) {
            
          return  _db.DeleteAsync(myformules);
        }

    }
}
